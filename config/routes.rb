ActionController::Routing::Routes.draw do |map|
  map.root :controller => 'dashboard'

  map.connect ':controller/:action'
  map.connect ':controller/:action.:format'
end
