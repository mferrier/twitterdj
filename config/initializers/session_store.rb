# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_twitterdj_session',
  :secret      => '0087c2b01154d5bf2601555c5d1a108699115129ff3ad3014b76a7dc8343d6e7198fc8d8db17fa61d9adc1405f2c9608381668659ea29403074597cd2840e47a'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
