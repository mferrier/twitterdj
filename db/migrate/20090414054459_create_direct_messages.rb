class CreateDirectMessages < ActiveRecord::Migration
  def self.up
    create_table :direct_messages do |t|
      t.integer :twitter_id
      t.string :sender_screen_name, :text
      t.timestamps
    end
  end

  def self.down
    drop_table :direct_messages
  end
end
