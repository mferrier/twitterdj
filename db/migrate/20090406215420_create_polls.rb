class CreatePolls < ActiveRecord::Migration
  def self.up
    create_table :polls do |t|
      t.belongs_to :event
      t.datetime :ended_at
      t.datetime :started_at
      t.boolean :finished, :default => false, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :polls
  end
end
