class CreateChoices < ActiveRecord::Migration
  def self.up
    create_table :choices do |t|
      t.string :identifier
      t.integer :track_database_id
      t.belongs_to :poll
      t.timestamps
    end
  end

  def self.down
    drop_table :choices
  end
end
