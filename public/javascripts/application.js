var TwitterDJ = {
  highlight_fontsize: '3em',
  normal_fontsize: '28px',

  refresh: function() {
    jQuery.getJSON('/dashboard/index.json', TwitterDJ.update_page);
  },

  update_page: function(data) {
    if (data.playing) { 
      TwitterDJ.update_current_track(data); 
    }
    if (data.poll_active) {

      // if we are on the last 20 seconds of the track
      if (parseInt($('#poll').attr('poll_id')) != data.poll_id) {
        TwitterDJ.highlight_winner(data);
        // if itunes has skipped to the next track, we can update the poll_id
        if (data.playing_last_track) {
          TwitterDJ.swap_polls(data);
        }
      } else if (data.playing_last_track) {
        // this is the "normal" loop, where iTunes is on the last track
        // of the playlist and we're running a poll        
        TwitterDJ.update_tracks(data);
      }
    }
  },
  
  update_current_track: function(data) {
    $('#time').text(data.time);
    $('#current_track').text(data.current_track);
  },
  
  highlight_winner: function(data) {
    winner_selector = '#' + data.previous_winner_dom_id;
    winning_choice = $(winner_selector);
    if (winning_choice.find('.track').css('fontSize') != TwitterDJ.highlight_fontsize) {
      winning_track = $(winning_choice.find('.track'));
      winning_track.animate({
        fontSize: TwitterDJ.highlight_fontsize
      }, 500);
      winning_bar = $(winning_choice.find('.bar'));
      winning_bar.animate({
        width: '100%'
      });
      loser_selector = ':not(' + winner_selector + ')';
      $('.choice').filter(loser_selector).each(function(){
        $(this).animate({
          opacity: '0.2'
        })
      });
    }
  },
  
  swap_polls: function(data) {
    TwitterDJ.stop_refresh_timer();
    $('#poll').slideUp(1000, function() {
      $('#poll').attr('poll_id', data.poll_id);
      $('.track').each(function(){
        $(this).css('fontSize', TwitterDJ.normal_fontsize);
        TwitterDJ.update_tracks(data);
      });
      $('.bar').each(function(){
        $(this).css('width', '0%');
      });
      $('.choice').each(function() {
        $(this).css('opacity', '1');
      });
      $('#poll').slideDown(500, function(){
        TwitterDJ.start_refresh_timer();
      });
      $('.vote').remove();
    });
  },
  
  update_tracks: function(data) {
    $('.choice').each(function(index) {
      choice = data.choices[index];
      $(this).find('.track').text(choice.description);
      if ($(this).find('.bar').css('width') != choice.bar_width) {
        $(this).find('.bar').animate({
          width: choice.bar_width
        }, 1000);
        $(this).find('.percentage').text(choice.vote_percentage);
      }
      $(this).attr('id', choice.dom_id);
    });
  },
  
  refresh_tweets: function() {
    since = $('.vote:first').attr('record_id');
    if (since == undefined) {
      since = 0;
    }
    jQuery.getJSON('/dashboard/tweets.json?since=' + since, TwitterDJ.update_tweets);
  },
  
  update_tweets: function(data) {
    console.log(data);
    jQuery.each(data.votes, function(){
      $('#tweets').prepend(
        "<div style='display: none' class='vote' record_id='" + this.record_id + "'>" +
        "<span class='user'>@" + this.twitter_user + "</span>" +
        " voted for " +
        "<span class='" + this.identifier.toLowerCase() + "> " + this.identifier.toUpperCase() + " </span>" +
        "</div>"
      );
      $(".vote[@record_id=" + this.record_id + "]").slideDown(500);
    });

  },
  
  start_refresh_timer: function() {
    $(document).everyTime(1500, 'refresh', TwitterDJ.refresh, 0, true);
    $(document).everyTime(5000, 'tweets', TwitterDJ.refresh_tweets, 0, true);    
  },
  
  stop_refresh_timer: function() {
    $(document).stopTime('refresh');
    $(document).stopTime('tweets');
  }
}

$(document).ready(function(){
  TwitterDJ.start_refresh_timer();
})