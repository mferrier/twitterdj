module DJ
  @@probably_protected_users = Set.new
  mattr_accessor :probably_protected_users

  extend self
  
  def run_forever
    loop do
      begin
        run()
      rescue Exception => e
        puts "error: #{e.message} #{e.backtrace.join("\n")}"
      end
      sleep 10
    end
  end
  
  def run
    puts "DJ cycling at #{Time.now}"
    return unless ITunes.playing?
    start_poll() unless current_poll
    follow_new_followers()
    process_votes()
    end_poll if poll_should_end?
  end
  
  def simulate_votes
    loop do
      ids = %w(a b c)
      DJ.vote(ids[rand(ids.size)], "fake")
      sleep 10
    end
  end
  
  def simulate_votes_old
    loop do
      master_ids = %w(a b c)
      master_ids.each do |preferred|
        puts "preferring #{preferred}"
        ids = ([preferred] * 4) + (master_ids - [preferred])
        10.times do
          DJ.vote(ids[rand(ids.size)], "fake" + rand(100).to_s)
          sleep 10
        end
      end
    end
  end
  
  # returns num random tracks that aren't already on the
  # system playlist
  def random_tracks(num = Poll::CHOICES_PER_POLL)
    available_tracks = (ITunes.pool_playlist_tracks - ITunes.system_playlist_tracks)
    (1..num).map do
      available_tracks.delete_at(rand(available_tracks.size))
    end
  end
  
  def current_poll
    Poll.current
  end
  
  def start_poll
    Poll.create(:custom_tracks => custom_tracks)
  end
  
  def end_poll
    # locally assign the current poll, then stop it
    poll = current_poll
    current_poll.stop()

    # add the winning track to the playlist so that
    # start_poll() won't consider the winning track in its
    # new poll
    ITunes.add_track(poll.winning_choice.track)
        
    # start a new poll
    start_poll()
  end
  
  def poll_should_end?
   ITunes.playing_last_track? && ITunes.seconds_left_in_current_track < 20 
  end
  
  # not thread safe
  def process_votes
    new_dms = twitter_connection.direct_messages(:since_id => DirectMessage.last_id.to_i)
    if new_dms.any?
      puts "#{new_dms.size} new DMs found"
      new_dms.each do |dm|
        # record DM even if it's not a vote, so we don't have to download it again.
        DirectMessage.create(:twitter_id => dm.id, :sender_screen_name => dm.sender_screen_name, :text => dm.text)
        vote(dm.text, dm.sender_screen_name)
      end
    else
      puts "No new DMs."
    end
  end


  def follow_new_followers
    (twitter_connection.follower_ids - twitter_connection.friend_ids - probably_protected_users.to_a).each do |user_id|
      begin
        twitter_connection.friendship_create(user_id)
      rescue
        puts "Couldn't follow user #{user_id}: #{$!.message}: #{$!.backtrace.join("\n")}"
        probably_protected_users << user_id
      end
      # wait for a bit to ease off on the rate limit
      sleep(0.25) 
    end
  end
  
  def twitter_connection
    @@twitter_connection ||= begin
      httpauth = Twitter::HTTPAuth.new(configatron.twitter.username, configatron.twitter.password)
      Twitter::Base.new(httpauth)
    end
  end
  
  def vote(identifier, twitter_user)
    identifier.strip!.upcase!
    choice = Poll.current.choices.find_by_identifier(identifier)
    if choice
      vote = choice.votes.build(:twitter_user => twitter_user)
      if vote.save
        puts "Saved vote for identifier '#{identifier}' by twitter user '#{twitter_user}' which maps to track '#{vote.choice.track.description}'."
        return true
      else
        puts "Couldn't save vote for identifier '#{identifier}' by twitter user '#{twitter_user}': #{vote.errors.full_messages.to_sentence}"
        return false
      end
    else
      puts "Couldn't find choice with identifier '#{identifier}' in current poll (Poll ##{Poll.current.id}) for twitter user '#{twitter_user}'. Discarding!"
      return false
    end
  end

  # tracks that the sysadmin would like to be included in the next poll
  def custom_tracks
    []
  end  
end
