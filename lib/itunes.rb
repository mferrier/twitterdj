require File.join(File.expand_path(File.dirname(__FILE__)), 'itunes', 'track')

module ITunes
  SYSTEM_PLAYLIST_NAME = configatron.itunes.system_playlist || 'twitterdj'
  POOL_PLAYLIST_NAME = configatron.itunes.pool_playlist || 'pool'
  extend self
  
  def app
    itunes_connection
  end

  def system_playlist
    create_system_playlist unless system_playlist_exists?
    playlists[SYSTEM_PLAYLIST_NAME]
  end

  def itunes_connection
    begin
      @@connection ||= Appscript.app('iTunes')
      @@connection.get # make sure iTunes is still around
      @@connection
    rescue Appscript::CommandError
      reconnect and retry
    end
  end
  
  def reconnect
    @@connection = Appscript.app('iTunes')
  end
  
  # all tracks in library
  def tracks
    @@tracks ||= playlists["Music"].tracks.database_ID.get.map{|i| Track.new(i)}
  end
  
  def create_system_playlist
    app.user_playlists.make(:new => :playlist, :with_properties => {:name => SYSTEM_PLAYLIST_NAME})
  end
  
  def system_playlist_exists?
    playlist_names.include?(SYSTEM_PLAYLIST_NAME)
  end
  
  def playlist_names
    playlists.keys
  end
  
  def playlists
    @@playlists ||= app.playlists.get.inject({}) do |playlists, list|
      playlists.merge(list.name.get => list)
    end
  end
  
  def add_track(track)
    track.ref.duplicate(:to => system_playlist)
  end
  
  def system_playlist_tracks
    system_playlist.tracks.database_ID.get.map{|i| Track.new(i)}
  end
  
  def pool_playlist_tracks
      pool_playlist.tracks.database_ID.get.map{|i| Track.new(i)}
  end

  def pool_playlist
    playlists[configatron.itunes.pool_playlist] || raise("Pool playlist #{configatron.itunes.pool_playlist} not found")
  end
  
  def playing?
    state == :playing
  end
  
  def playing_last_track?
    playing? && current_track == system_playlist_tracks.last
  end
  
  def state
    app.player_state.get
  end
  
  def current_track
    if playing?
      Track.new(app.current_track.database_ID.get)
    else
      nil
    end
  end
  
  def seconds_left_in_current_track
    (current_track.duration - app.player_position.get)
  end
  
  def time_left_in_current_track
    seconds = seconds_left_in_current_track
    "%i:%02i" % [seconds / 60, seconds % 60]
  end
end
