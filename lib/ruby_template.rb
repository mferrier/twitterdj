# Allow *.rb templates where the last expression is the output

class RubyTemplate < ActionView::TemplateHandler
  include ActionView::TemplateHandlers::Compilable

  def compile(template)
    template.source
  end
end
ActionView::Template.register_template_handler(:rb, RubyTemplate)
