class ActiveRecord::Base
  def dom_id
    [self.class.to_s.underscore, self.id].join('_')
  end
end