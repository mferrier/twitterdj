module ITunes
  # represents a track in iTunes, represented by its unique iTunes database_ID.
  # lazy-loads its appscript reference, from which it can glean all other important
  # details. 
  class Track
    include Appscript # required for using the "its" filtering in #ref
    
    attr_reader :database_id
    
    def initialize(database_id)
      @database_id = database_id
    end
    
    def ref
      @ref ||= ITunes.app.tracks[its.database_ID.eq(@database_id)].get.first
    end
    
    def hash
      @database_id.hash
    end
    
    def eql?(other)
      self == other
    end
    
    def ==(other)
      if other.is_a?(ITunes::Track)
        @database_id == other.database_id
      else
        super
      end
    end
  
    def title
      ref.name.get
    end
  
    def artist
      ref.artist.get
    end
  
    def album
      ref.album.get
    end
  
    def time
      ref.time.get
    end
  
    def duration
      ref.duration.get.to_i
    end
  
    def description
      "#{artist} - #{title}"
    end
    
    def playing?
      self == ITunes.current_track
    end
  end
end