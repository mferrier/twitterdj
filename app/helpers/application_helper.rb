# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def to_json(val)
    # uncomment after development
    #JSON.send(Rails.env == 'production' ? 'generate' : 'pretty_generate', val)
    
    # for now, always make it pretty so that we can run the staging server in production mode
    JSON.pretty_generate(val)
  end  
end
