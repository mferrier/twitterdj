class DashboardController < ApplicationController
  def index
    @tracks = ITunes.system_playlist_tracks
    @poll = Poll.current
    
    respond_to do |format|
      format.html {}
      format.json {}
    end
  end
  
  def tweets
    @votes = Poll.current.recent_votes_since(params[:since]).reverse
    respond_to do |format|
      format.json {}
    end
  end 
end
