# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
  after_filter :render_json_as_plain_text
  

  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
  
  # render json as plain content in dev mode so
  # that we can look at it in browser
  def render_json_as_plain_text
    # uncomment after json development
    # if Rails.env == 'development' && response.content_type == 'application/json'
    #       response.content_type = 'text/plain'
    #     end
    
    # for now, always render as plain text so that we can run the staging server in production mode
    if response.content_type == 'application/json'
      response.content_type = 'text/plain'
    end
  end
end
