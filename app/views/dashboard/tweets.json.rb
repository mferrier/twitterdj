to_json({
  :votes => @votes.map do |vote|
    {
      :twitter_user => vote.twitter_user,
      :identifier => vote.choice.identifier,
      :record_id => vote.id
    }
  end
})