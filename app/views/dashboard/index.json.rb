to_json({
  :playing => ITunes.playing?,
  :time => (ITunes.time_left_in_current_track if ITunes.playing?),
  :current_track => (ITunes.current_track.description if ITunes.playing?),  
  :poll_active => !!@poll,
  :poll_id => (@poll.id if @poll),
  :previous_winner_dom_id => (Poll.previous.winning_choice.dom_id if Poll.previous),
  :playing_last_track => ITunes.playing_last_track?,
  :votes => @poll.recent_votes.map do |vote|
    {
      :twitter_user => vote.twitter_user,
      :identifier => vote.choice.identifier
    }
  end,
  :choices => (@poll.choices.map do |choice|
    {
      :dom_id => choice.dom_id,
      :description => "#{choice.identifier}. #{choice.track.description}",
      :vote_percentage => number_to_percentage(choice.vote_percentage, :precision => 0),
      :bar_width => (choice.vote_percentage * 10).to_s + "px"
    }
  end if @poll)
})