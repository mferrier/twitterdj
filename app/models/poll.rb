class Poll < ActiveRecord::Base
  belongs_to :event
  has_many :choices, :dependent => :destroy
  has_many :votes, :through => :choices
  
  before_create :create_choices

  # DJ will put custom preferred tracks in here and create_choices
  # will incorporate
  attr_accessor :custom_tracks
  
  CHOICES_PER_POLL = configatron.poll.choices_per || 3
  
  def create_choices
    tracks = custom_tracks + DJ.random_tracks(CHOICES_PER_POLL - custom_tracks.size)
    tracks.each_with_index do |track, i|
      choices.build(:track_database_id => track.database_id, :identifier => Choice::IDENTIFIERS[i])
    end
  end
  
  def winning_choice
    choices.first(
      :select => 'choices.*, count(votes.id) AS vote_count',
      :joins => "INNER JOIN votes ON votes.choice_id = choices.id", 
      :group => "choices.id",
      :order => 'vote_count desc'
    ) || choices.first
  end
  
  def recent_votes
    votes.find(:all, :conditions => ['twitter_user != ?', 'fake'], :order => 'votes.created_at desc', :limit => 9)    
  end
  
  def recent_votes_since(since)
    votes.find(:all, :conditions => ['twitter_user != ? AND votes.id > ?', 'fake', since.to_i], :order => 'votes.created_at desc', :limit => 9)
  end
  
  def vote_percentage_for_choice(choice)
    return 0 if votes.count == 0
    ((choice.votes.count / votes.count.to_f) * 100).round
  end
  
  def stop
    update_attributes(:finished => true, :ended_at => Time.now)
  end
  
  class << self
    def current
      first(:order => 'created_at desc', :conditions => ['finished = ?', false])
    end
    
    def previous
      first(:order => 'ended_at desc', :conditions => ['finished = ?', true])
    end
  end
end
