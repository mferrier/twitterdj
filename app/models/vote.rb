class Vote < ActiveRecord::Base
  belongs_to :choice
  has_one :poll, :through => :choice
  
  validates_presence_of :twitter_user
end
