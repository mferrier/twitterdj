class Choice < ActiveRecord::Base
  belongs_to :poll
  has_many :votes, :dependent => :destroy
  
  IDENTIFIERS = ('A'..'Z').to_a.freeze
  
  def track
    ITunes::Track.new(track_database_id)
  end
  
  def winning?
    self == poll.winning_choice
  end
  
  def vote_percentage
    poll.vote_percentage_for_choice(self)
  end
end
