class DirectMessage < ActiveRecord::Base
  class << self
    def last_id
      connection.select_value("SELECT twitter_id FROM direct_messages ORDER BY twitter_id DESC LIMIT 1")
    end
  end
end
